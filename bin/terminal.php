#!/usr/bin/env php
<?php

use KarlitoWeb\Console\src\Command\ExportFilesListCommand;
use KarlitoWeb\Console\src\Command\Templates;
use Symfony\Component\Console\Application;
use Symfony\Component\Console\Input\ArgvInput;
use Symfony\Component\Console\Output\ConsoleOutput;

/** Check CLI only */
if (!in_array(PHP_SAPI, ['cli', 'phpdbg', 'embed'], true)) {
    echo 'Warning: The console should be invoked via the CLI version of PHP, not the ' . PHP_SAPI . ' SAPI' . PHP_EOL;
}

/** Load Libraries */
require dirname(__DIR__) . '/vendor/autoload.php';

/** InputInterface */
$input  = new ArgvInput();

/** OutputInterface */
$output = new ConsoleOutput();


try {
	if (null !== $env = $input->getParameterOption(['--env', '-e'], null, true)) {
		putenv('APP_ENV='.$_SERVER['APP_ENV'] = $_ENV['APP_ENV'] = $env);
	}

	if ($input->hasParameterOption('--no-debug', true)) {
		putenv('APP_DEBUG='.$_SERVER['APP_DEBUG'] = $_ENV['APP_DEBUG'] = '0');
	}

	/** Set */
	$application = new Application();
	$application->setName('Karlito Web :: Console');
	$application->setVersion('4.0.0');

	/** Add */
	$application->addCommands([
        new ExportFilesListCommand(),
        new Templates(),
	]);

	/** Run app */
	$application->run($input, $output);
} catch (\Exception $exception) {
	$output->write("Message : " . $exception->getMessage());
	$output->write(null, 2);
	$output->write("getCode() : " . $exception->getCode());
	$output->write(null, 2);
	$output->write("__toString() : " . $exception->__toString());
}
