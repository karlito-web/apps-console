<?php

namespace KarlitoWeb\Console\src\Feature;

use Symfony\Component\Console\Helper\Table;
use Symfony\Component\Console\Output\OutputInterface;

trait Render
{
	/**
	 * Provides helpers to display a table.
	 *
	 * @param array           $headers
	 * @param array           $content
	 * @param OutputInterface $output
	 * @return Table
	 * @example :
	 * $headers = [
	 *     [20, 40, 40],                // $headers
	 *     ['lorem', 'ipsum', 'ipsum'], // $datas
	 * ];
	 */
	public static function table(array $headers, array $content, OutputInterface $output): Table
	{
		$nb                   = count($content);
		list($width, $title)  = $headers;
		$table                = new Table($output);
		$table->setColumnWidths($width);
		$table->setHeaders($title);
		$table->setStyle('borderless');
		switch ($nb):
			case 1 :
				$table->setRows([$content]);
				break;
			case ($nb > 1):
				$table->setRows($content);
				break;
		endswitch;
		$table->render();

		return $table;
	}
}
