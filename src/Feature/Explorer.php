<?php

namespace KarlitoWeb\Console\src\Feature;

use GlobIterator;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;

/**
 * @method getHelper(string $string)
 */
trait Explorer
{
	/**
	 * @param string $sourcePath
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return string
	 */
	private function getDirectory(string $sourcePath, InputInterface $input, OutputInterface $output): string
	{
		// Obtenez la liste des dossiers dans le répertoire
		$directories = glob($sourcePath . '\\*', GLOB_ONLYDIR);
		$directories = str_replace($sourcePath . DIRECTORY_SEPARATOR, '', $directories);

		/** Questions */
		$helper = $this->getHelper('question');
		$dir    = new ChoiceQuestion('Select a Template', $directories, 0);

		/** Answers */
		return $helper->ask($input, $output, $dir);
	}

	/**
	 * @param string $sourcePath
	 * @return array<GlobIterator>
	 */
	private static function getHTMLFiles(string $sourcePath): array
	{
		$files    = [];
		$fileIterator = new GlobIterator($sourcePath);
		foreach ($fileIterator as $item) {
			// Filter
			if ($item->isFile() && $item->getExtension() === 'html') {
				$files[] = $item;
			}
		}
		return $files;
	}
}
