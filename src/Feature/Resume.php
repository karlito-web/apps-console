<?php

namespace KarlitoWeb\Console\src\Feature;

use Symfony\Component\Console\Style\SymfonyStyle;

trait Resume
{
	/**
	 * Display the resume of CLI Command
	 * @param SymfonyStyle $io
	 * @param int          $duration
	 */
	public static function command(SymfonyStyle $io, int $duration = 0): void
	{
		$io->newLine(3);
		$io->section('Resume                        ');
		/* Currently used memory */
		$mem_usage = memory_get_usage();
		/* Peak memory usage */
		$mem_peak  = memory_get_peak_usage();
		if ($duration > 180) {
			$time = 'Excection Time      : ' . round($duration / 60) . ' min.';
		} else {
			$time = 'Excection Time      : ' . $duration . ' sec.';
		}
		$io->text([
			'<fg=green;bg=default>' . $time . '</>',
			'<fg=red;bg=white>The script is using : ' . round($mem_usage / 1048576) . ' MB of memory.</>',
			'<fg=red;bg=white>Peak usage          : ' . round($mem_peak / 1048576) . ' MB of memory.</>',
		]);
	}
}
