<?php

namespace KarlitoWeb\Console\src\Feature;

use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;

trait Common
{
	/** Configure your CLI Application */
	protected function configure(): void
    {
		$this->setName(self::$name);
		$this->setDescription(self::$desc);
		$this->setHelp(self::$help);
		$this->setHidden(self::$hidden);
		$this->setAliases(self::$aliases);
	}

	/**
	 * @param InputInterface  $input
	 * @param OutputInterface $output
	 */
	protected function initialize(InputInterface $input, OutputInterface $output): void
	{
		/** Set Options */
		ini_set("memory_limit", "-1");
		set_time_limit(0);

		/** Clear Screen */
		$output->writeln(shell_exec('clear'));
	}
}
