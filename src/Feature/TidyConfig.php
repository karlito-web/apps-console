<?php

namespace KarlitoWeb\Console\src\Feature;

trait TidyConfig
{
    /** @return array */
    private static function getConfig(): array
    {
        return [
            'add-meta-charset' => true,
            'doctype'          => 'html5',
            'output-xhtml'     => true,
            'clean'            => true,
            'indent'           => true,
            'indent-with-tabs' => true,
            'keep-tabs'        => true,
            'sort-attributes'  => 'alpha',
            'tab-size'         => 4,
            'wrap'             => 180,
        ];
    }
}
