<?php

namespace KarlitoWeb\Console\src\Command;

use DateTime;
use DateTimeZone;
use Exception;
use JetBrains\PhpStorm\NoReturn;
use KarlitoWeb\Console\src\Feature\Common;
use KarlitoWeb\Console\src\Feature\Explorer;
use KarlitoWeb\Console\src\Feature\Resume;
use KarlitoWeb\Console\src\Feature\TidyConfig;
use KarlitoWeb\Toolbox\File\Directory;
use KarlitoWeb\Toolbox\File\File;
use KarlitoWeb\Toolbox\PhpToHtmlTidy\StringToHTML;
use KarlitoWeb\Toolbox\Yaml\YamlToArray;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Input\InputOption;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Filesystem\Exception\IOException;
use Symfony\Component\Filesystem\Filesystem;
use Symfony\Component\Stopwatch\Stopwatch;

class Templates extends Command
{
	use Common;
	use Explorer;
	use Resume;
	use TidyConfig;

	const EXTENSION              = ['html', 'twig'];

	private static string $name  = 'template';

	private static string $desc  = '';

	private static string $title = '::::: Templates :::::';

	private static string $help  = '';

	private static bool $hidden  = false;

	private static array $aliases = [
		'templates',
	];

	/**
	 * @throws Exception
	 */
	#[NoReturn]
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** Question */
		$extension    = ($input->getOption('extension') === false) ?
			self::EXTENSION :
			$this->questionExtension($input, $output)
		;

        /** Define Directories */
		$ds                 = DIRECTORY_SEPARATOR;
		$documents          = dirname(__FILE__, 3) . $ds . 'documents' . $ds;
		$yamlDir            = $documents . 'yaml' . $ds;
		$start              = 'G:' . $ds . 'Templates' . $ds;
		$source	    	    = $start . 'Sources' . $ds;
		$directoryName      = self::getDirectory($source, $input, $output);
		$directory          = $source . $directoryName . $ds;
		$templateName       = self::getDirectory($directory, $input, $output);
		$source             = $source . $directoryName . $ds . $templateName . $ds;
		$destination        = $start . 'Destinations' . $ds . $directoryName . $ds . $templateName . $ds;
		$sourceAssets       = $source . 'assets' . $ds;
		$destinationAssets  = $destination . 'assets' . $ds;

        /** Init variables */
		$io           = new SymfonyStyle($input, $output);
		$fs           = new Filesystem();
		$date         = new DateTime('now', new DateTimeZone('Europe/Paris'));
		$name         = $this->setName(self::$name);
		$config       = self::getConfig();
		$patterns     = YamlToArray::generate($yamlDir . 'oneline-patterns.yaml');
		$replacements = YamlToArray::generate($yamlDir . 'oneline-replacements.yaml');

        /** Execution time : start */
        $stopwatch    = new Stopwatch(TRUE);
        $stopwatch->start($name->getName());

		/** Handler */
		$io->title(self::$title);

		// Create Destination Directory
		Directory::makeDirectory($destination, '0775');
		Directory::makeDirectory($destinationAssets, '0775');

		// Copy Assets
		try {
			$fs->mirror($sourceAssets, $destinationAssets);
		} catch (IOException $e) {
			// throw new IOException($e->getMessage());
			dump($e->getMessage());
		}

		// Get List of HTML Files
		$files       = self::getHTMLFiles($source . 'html' . $ds . '*.html');
		foreach ($files as $file) {
			// Read HTML File
			$read = File::readFile($file->getRealPath());
			// Delete or Replace
			$delete    = preg_replace($patterns['delete'], $replacements['delete'], $read);
			$replace   = preg_replace($patterns['replace'], $replacements['replace'], $delete);
			// Tidy String
			$html      = StringToHTML::generate($replace, $config, 'utf8');
			// Create Files
			if (is_array($extension)) {
				foreach ($extension as $value) {
					$datas = [
						'to' => $destination . $value . $ds . str_replace($file->getExtension(), '', $file->getFilename()) . $value,
						'html' => $html,
					];
					self::createHTMLFile($datas);
				}
			} else {
				$datas = [
					'to' => $destination . $extension . $ds . str_replace($file->getExtension(), '', $file->getFilename()) . $extension,
					'html' => $html,
				];
				self::createHTMLFile($datas);
			}
		}

		/** Execution time : stop */
		$event       = $stopwatch->stop($name->getName());
		$duration    = $event->getDuration() / 1000;

		/** Resume */
		$io->newLine(3);
		$io->section('Resume');
		$io->text($duration);

		return Command::SUCCESS;
	}

	/** Configure your CLI Application */
	protected function configure(): void
    {
		$this->setName(self::$name);
		$this->setDescription(self::$desc);
		$this->setHelp(self::$help);
		$this->setHidden(self::$hidden);
		$this->setAliases(self::$aliases);
		$this->addOption('extension', 'ext', InputOption::VALUE_NONE, 'Choose an output extension');
		$this->addOption('replace', 'rpl', InputOption::VALUE_OPTIONAL, 'Choose a replacement config');
	}

	private function questionExtension(InputInterface $input, OutputInterface $output): string
	{
		/** Questions */
		$helper = $this->getHelper('question');
		$dir    = new ChoiceQuestion('Select an Extension', self::EXTENSION, 0);

		/** Answers */
		return $helper->ask($input, $output, $dir);
	}

	#[NoReturn]
	private static function createHTMLFile(array $datas): void
	{
		extract($datas);
		if (File::createFile($to, $html) === false) {
			exit('impossible to create HTML File');
		}
	}
}
