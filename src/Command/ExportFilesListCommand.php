<?php

namespace KarlitoWeb\Console\src\Command;

use DateTime;
use DateTimeZone;
use Exception;
use KarlitoWeb\Console\src\Feature\Common;
use KarlitoWeb\Console\src\Feature\Resume;
use KarlitoWeb\Toolbox\Csv\ArrayToCsv;
use KarlitoWeb\Toolbox\File\Directory;
use KarlitoWeb\Toolbox\File\File;
use KarlitoWeb\Toolbox\Yaml\ArrayToYaml;
use League\Csv\CannotInsertRecord;
use League\Csv\InvalidArgument;
use League\Csv\UnavailableStream;
use Symfony\Component\Console\Command\Command;
use Symfony\Component\Console\Input\InputInterface;
use Symfony\Component\Console\Output\OutputInterface;
use Symfony\Component\Console\Question\ChoiceQuestion;
use Symfony\Component\Console\Question\Question;
use Symfony\Component\Console\Style\SymfonyStyle;
use Symfony\Component\Finder\Finder;
use Symfony\Component\Stopwatch\Stopwatch;

class ExportFilesListCommand extends Command
{
	use Common;
	use Resume;

	private static string $name  = 'export:files:list';

	private static string $desc  = 'List Files Exporter : Export a list of files in a CSV file for a directory';

	private static string $title = '::::: List Files Exporter :::::';

	private static string $help  = 'Set the directory to explore';

	private static bool $hidden  = false;

	private static array $aliases = [
		'export-files-list',
	];

	/**
	 * @param InputInterface $input
	 * @param OutputInterface $output
	 * @return int
	 * @throws Exception
	 */
	protected function execute(InputInterface $input, OutputInterface $output): int
	{
		/** Init variables */
		$io        = new SymfonyStyle($input, $output);
		$date      = new DateTime('now', new DateTimeZone('Europe/Paris'));
		$ds        = DIRECTORY_SEPARATOR;
		$filename  = $date->format('Y-m-d') . '__' . $date->format('H') . 'h' . $date->format('i');
//		$pathInit  = dirname(getcwd()) . $ds; // "E:\PHP\Apps\"
		$path      = "E:\- documents -" . $ds . 'Exports' . $ds . 'FileList' . $ds;

		/** Questions */
		$helper    = $this->getHelper('question');
	    $q1        = new Question('Directory to scan : ', false);
	    $q2        = new ChoiceQuestion('Output Extension  : ', ['csv', 'yaml'], 0);

		/** Answers */
	    $directory = $helper->ask($input, $output, $q1);
	    $extension = $helper->ask($input, $output, $q2);

		/** Output File */
		$filepath  = $path . $filename . '.' . $extension;

		/** Handler */
		$io->title(self::$title);

		/** Execution time : start */
		$stopwatch  = new Stopwatch(TRUE);
		$stopwatch->start('exporter');

		/** Scan directory */
        $result     = self::scan($directory);

		Directory::makeDirectory($path);
		File::createFile($filepath);

		/** Export Result */
		if ($extension === 'csv') {
			// to CSV file
			$header     = self::headers();
			try {
				ArrayToCsv::generate($filepath, $header, $result);
			} catch (CannotInsertRecord|InvalidArgument|UnavailableStream $e) {
				echo $e->getMessage();
			}
		} elseif($extension === 'yaml') {
			// to YAML file
			try {
				ArrayToYaml::generate($filepath, $result, true);
			} catch (Exception $e) {
				echo $e->getMessage();
			}
		} else {
			$io->error('Error with : ' . $filepath);
			return Command::FAILURE;
		}

		/** Execution time : stop */
        $event      = $stopwatch->stop('exporter');
        $duration   = $event->getDuration() / 1000;

		/** Resume */
		$io->section('Directory Scanned');
		$io->text($directory);
		$io->newLine(2);
		$io->section('Output File');
		$io->text($filepath);
		$io->newLine(4);
        // self::command($io, $duration);

		return Command::SUCCESS;
	}

	/** @return array */
	private static function headers(): array
	{
		return [
			'path',
			'filename',
			'filenameWithoutExtension',
			'pathname',
			'extension',
			'aTime',
			'mTime',
			'cTime',
			'size',
			'perms',
			'owner',
			'group',
			'type',
			'writable',
			'readable',
			'executable',
			'file',
			'dir',
			'link',
		];
	}

	/**
	 * @param string $directory
	 * @return array
	 */
	private static function scan(string $directory): array
	{
		$result = [];
		$finder = new Finder();
		$finder
			->files()
			->ignoreUnreadableDirs()
			->ignoreVCS(true)
			->in($directory)
			->sortByType()
		;

		/** @var Finder $file */
		foreach ($finder as $file) {
			$result[] = [
				'path'                     => $file->getFileInfo()->getPath(),
				'filename'                 => $file->getFileInfo()->getFilename(),
				'filenameWithoutExtension' => $file->getFilenameWithoutExtension(),
				'pathname'                 => $file->getFileInfo()->getPathname(),
				'extension'                => $file->getFileInfo()->getPath(),
				'aTime'                    => $file->getFileInfo()->getATime(), // date du dernier acces au fichier
				'mTime'                    => $file->getFileInfo()->getMTime(), // date de la derniere modification
				'cTime'                    => $file->getFileInfo()->getCTime(), // date de la modification du fichier
				'size'                     => $file->getFileInfo()->getSize(),
				'perms'                    => $file->getFileInfo()->getPerms(),
				'owner'                    => $file->getFileInfo()->getOwner(),
				'group'                    => $file->getFileInfo()->getGroup(),
				'type'                     => $file->getFileInfo()->getType(),
				'writable'                 => $file->getFileInfo()->isWritable(),
				'readable'                 => $file->getFileInfo()->isReadable(),
				'executable'               => $file->getFileInfo()->isExecutable(),
				'file'                     => $file->getFileInfo()->isFile(),
				'dir'                      => $file->getFileInfo()->isDir(),
				'link'                     => $file->getFileInfo()->isLink(),
			];
		}

		return $result;
	}
}
