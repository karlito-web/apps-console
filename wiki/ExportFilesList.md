## List Files Exporter
#### By Karlito Web  

***
### Description:  
Export a list of files in a CSV file for a directory.

***
### Usage:  
Open CLI and execute 
```cli 
php bin/terminal.php export:files:list
```

```cli 
php bin/terminal.php export-files-list
```
