# Karlito Web :: Console

###### Description:  


***
###### Table of Contents: 

1. [ExportFilesList](wiki/ExportFilesList.md)
1. [Templates](wiki/Templates.md)

***
###### Installation:
```cli 
composer require karlito-web/apps-console
```

***
###### Usage:  
Open CLI and execute 
```cli 
php bin/terminal.php [APP:NAME]
```

The list of command 
```cli 
php bin/terminal.php list
```

The helper 
```cli 
php bin/terminal.php help
```

***
###### Contributing:  
Larger projects often have sections on contributing to their project, in which contribution instructions are outlined. Sometimes, this is a separate file. If you have specific contribution preferences, explain them so that other developers know how to best contribute to your work. To learn more about how to help others contribute, check out the guide for setting guidelines for repository contributors.

***
###### Credits:  
Include a section for credits in order to highlight and link to the authors of your project.

***
###### License:  
Finally, include a section for the license of your project. For more information on choosing a license, check out GitHub’s licensing guide!
